package main

import (
	pb "bitbucket.org/gophersmicroservices/user-service/proto/user"
	"database/sql"
	"fmt"
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"log"
)

type Config struct {
	PostgresURI string
	JWTSecret string
}

func main() {
	var config Config
	srv := micro.NewService(
		micro.Name("go.micro.srv.user"),
		micro.Version("latest"),
		micro.Flags(cli.StringFlag{
			Name:        "postgres_uri",
			Value:       "postgres://test:test@localhost:5432",
			Usage:       "Postgres connection string",
			EnvVar:      "POSTGRES_URI",
			Destination: &config.PostgresURI,
		}, cli.StringFlag{
			Name:        "jwt_secret",
			Value:       "sometestkey",
			Usage:       "JWT secret for signing token",
			EnvVar:      "JWT_SECRET",
			Destination: &config.JWTSecret,
		}),
	)
	srv.Init()

	db, err := sql.Open("postgres", config.PostgresURI)
	if err != nil {
		log.Fatalf("could not connect to Postgres server: %v",err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalf("could not ping Postgres db server: %v", err)
	}

	defer func () {
		if err := db.Close(); err != nil {
			log.Fatalf("fail when close postgres connection: %v", err)
		}
	}()

	repo, err := NewUserRepository(db)
	if err != nil {
		log.Fatalf("fail when create sql table: %v", err)
	}


	tokenService := NewTokenService(config.JWTSecret)
	pb.RegisterUserServiceHandler(srv.Server(), &service{repo, tokenService})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
