module bitbucket.org/gophersmicroservices/user-service

require (
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-log/log v0.1.0 // indirect
	github.com/golang/protobuf v1.2.0
	github.com/google/uuid v1.1.0 // indirect
	github.com/hashicorp/consul v1.4.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.0.0-20150518234257-fa3f63826f7c // indirect
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/hashicorp/go-rootcerts v0.0.0-20160503143440-6bb64b370b90 // indirect
	github.com/hashicorp/go-sockaddr v0.0.0-20180320115054-6d291a969b86 // indirect
	github.com/hashicorp/memberlist v0.1.0 // indirect
	github.com/hashicorp/serf v0.8.1 // indirect
	github.com/lib/pq v1.0.0
	github.com/micro/cli v0.0.0-20181223120019-751f61337558
	github.com/micro/go-log v0.0.0-20170512141327-cbfa9447f9b6 // indirect
	github.com/micro/go-micro v0.14.1
	github.com/micro/go-rcache v0.0.0-20180418165751-a581a57b5133 // indirect
	github.com/micro/h2c v1.0.0 // indirect
	github.com/micro/mdns v0.0.0-20181201230301-9c3770d4057a // indirect
	github.com/micro/util v0.0.0-20181115195001-2d4f591dc538 // indirect
	github.com/miekg/dns v1.1.1 // indirect
	github.com/mitchellh/hashstructure v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/sean-/seed v0.0.0-20170313163322-e2103e2c3529 // indirect
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3
	golang.org/x/sys v0.0.0-20181221143128-b4a75ba826a6 // indirect
	golang.org/x/text v0.3.0 // indirect
)
