package main

import (
	pb "bitbucket.org/gophersmicroservices/user-service/proto/user"
	"database/sql"
	_ "github.com/lib/pq"
)


type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) (*UserRepository,error) {
	r :=  &UserRepository{
		db:db,
	}

	if err := r.createTable("users"); err != nil {
		return nil, err
	}

	return r, nil
}

func (repo *UserRepository) createTable(name string) error {
	const qry = `CREATE TABLE IF NOT EXISTS users (
		id serial PRIMARY KEY,
		password text,
		email text,
		company text,
		name text
	)`

	if _, err := repo.db.Exec(qry); err != nil {
		return err
	}

	return nil
}

func (repo *UserRepository) GetAll() ([]*pb.User, error) {
	rows, err := repo.db.Query("SELECT id,email,company,name FROM users")
	if err != nil {
		return nil, err
	}

	var users[]*pb.User
	for rows.Next() {
		user := &pb.User{}
		if err := rows.Scan(&user.Id, &user.Email, &user.Company, &user.Name); err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}

func (repo *UserRepository) Get(id int64) (*pb.User, error) {
	user := &pb.User{}
	err := repo.db.QueryRow("SELECT id,email,company,name FROM users WHERE id=$1", id).Scan(&user.Id, &user.Email, &user.Company, &user.Name)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (repo *UserRepository) GetByEmail(email string) (*pb.User, error) {
	user := &pb.User{}
	err := repo.db.QueryRow("SELECT * FROM users WHERE email=$1", email).Scan(&user.Id, &user.Password, &user.Email, &user.Company, &user.Name)

	if err != nil {
		return nil, err
	}

	return user, nil
}


func (repo *UserRepository) Create(user *pb.User) error {
	sqlInsertStatement := `INSERT INTO users (password, name, company, email) VALUES ($1, $2, $3, $4)`
	_, err := repo.db.Exec(sqlInsertStatement, user.Password, user.Name, user.Company, user.Email)
	return err
}