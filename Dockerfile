FROM golang:1.11 as builder
WORKDIR /app/user-service
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/user-service

FROM alpine:latest

WORKDIR /app
COPY --from=builder /app/user-service/bin/user-service .
CMD ["./user-service"]