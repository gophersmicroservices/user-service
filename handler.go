package main

import (
	pb "bitbucket.org/gophersmicroservices/user-service/proto/user"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
)

type Authable interface {
	Encode(*pb.User) (string, error)
	Decode(tokenString string)(*CustomClaims, error)
}

type Repository interface {
	GetAll() ([]*pb.User, error)
	Get(id int64) (*pb.User, error)
	Create(user *pb.User) error
	GetByEmail(email string) (*pb.User, error)
}

type service struct {
	repo Repository
	tokenService Authable
}

func (srv *service) Get(ctx context.Context, req *pb.User, res *pb.Response) error {
	user, err := srv.repo.Get(req.Id)
	if err != nil {
		return err
	}
	res.User = user
	return nil
}

func (srv *service) GetAll(ctx context.Context, req *pb.Request, res *pb.Response) error {
	users, err := srv.repo.GetAll()
	if err != nil {
		return err
	}
	res.Users = users
	return nil
}

func (srv *service) Auth(ctx context.Context, req *pb.User, res *pb.Token) error {
	user, err := srv.repo.GetByEmail(req.Email)
	if err != nil {
		return err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return err
	}

	token, err := srv.tokenService.Encode(user)
	if err != nil {
		return err
	}

	res.Token = token
	return nil
}

func (srv *service) Create(ctx context.Context, req *pb.User, res *pb.Response) error {
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	req.Password = string(hashedPass)
	if err := srv.repo.Create(req); err != nil {
		return err
	}

	token, err := srv.tokenService.Encode(req)
	if err != nil {
		return err
	}

	res.Token = &pb.Token{
		Token:                token,
	}

	res.User = &pb.User{}
	res.User.Name = req.Name
	res.User.Email = req.Email
	res.User.Company = req.Company
	return nil
}

func (srv *service) ValidateToken(ctx context.Context, req *pb.Token, res *pb.Token) error {
	claims, err := srv.tokenService.Decode(req.Token)

	if err != nil {
		return err
	}

	if claims.User.Id == 0 {
		return errors.New("invalid user")
	}

	res.Valid = true
	return nil
}